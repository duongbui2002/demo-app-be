import * as mongoose from "mongoose";
import { DEFAULT_ACCOUNT_EMAIL, DEFAULT_ACCOUNT_PASSWORD, DEFAULT_ACCOUNT_USERNAME, MONGO_URI } from "@configs/env";
import { Model } from "mongoose";
import { Logger } from "@nestjs/common";
import { AccountDocument } from "@modules/accounts/account.schema";

export class DefaultAccount {
  private connection;
  private accountModel: Model<AccountDocument>;

  constructor(AccountSchema) {
    this.accountModel = mongoose.model("accounts", AccountSchema);
  }

  generate = async () => {
    mongoose.set("strictQuery", false);
    this.connection = await mongoose.connect(MONGO_URI);
    if (await this.accountModel.countDocuments({}) <= 0) {
      try {
        await this.accountModel.create({
          username: DEFAULT_ACCOUNT_USERNAME,
          displayName: DEFAULT_ACCOUNT_USERNAME,
          email: DEFAULT_ACCOUNT_EMAIL,
          password: DEFAULT_ACCOUNT_PASSWORD,
          roles: ["admin", "user"],
          isActivated: true
        });
      } catch (e) {
        Logger.error(e);
      }
      await mongoose.disconnect();
    }
  };
}
