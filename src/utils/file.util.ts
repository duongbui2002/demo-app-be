export const getExtension = async (name: string) => {
  const fileType = await import("file-type");
  return fileType.fileTypeFromFile(name);
};
