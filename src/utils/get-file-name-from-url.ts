export function getFileNameFromUrl(url: string): string {
  const splitUrl = url.split('/')
  const fileName = splitUrl[splitUrl.length - 1]
  return fileName;
}
