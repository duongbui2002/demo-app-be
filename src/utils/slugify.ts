import { Model } from "mongoose";

export const slugify = (str: string) => {
  return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/đ/g, "d").replace(/Đ/g, "D").replace(/ /g, "-").toLowerCase().replace(/[^\w-]+/g, "");
};

export const uniqueSlugGenerator = async (value: string, model: any, exception?: any): Promise<string> => {
  let count = 0, newSlug = slugify(value);

  while (await model.countDocuments((!exception) ? { slug: newSlug } : { slug: newSlug, ...exception }) > 0) {
    newSlug = `${slugify(value)}_${++count}`;
  }

  return newSlug;
};
