require("module-alias/register");
import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { Logger, ValidationPipe } from "@nestjs/common";
import * as compression from "compression";
import * as bodyParser from "body-parser";
import rateLimit from "express-rate-limit";
import { PORT, RATE_LIMIT_MAX, MONGO_URI } from "@configs/env";
import * as mongoSanitize from "express-mongo-sanitize";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalPipes(new ValidationPipe({
    whitelist: true,
    transform: true,
    disableErrorMessages: false
  }));

  // NOTE: compression
  app.use(compression());

  // NOTE: body parser
  app.use(bodyParser.json({ limit: "50mb" }));

  app.use(
    bodyParser.urlencoded({
      limit: "50mb",
      extended: true,
      parameterLimit: 50000
    })
  );

  app.use(bodyParser.json());

  app.use(
    mongoSanitize({
      replaceWith: "_"
    })
  );

  // NOTE: rateLimit
  app.use(
    rateLimit({
      windowMs: 1000 * 60 * 60, // an hour
      max: RATE_LIMIT_MAX || 10000, // limit each IP to 100 requests per windowMs
      message:
        "⚠️Too many request created from this IP, please try again after an hour"
    })
  );

  app.enableCors({
    origin: "*",
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    preflightContinue: false,
    optionsSuccessStatus: 204,
    credentials: true
  });
  console.log("URI:",MONGO_URI);

  await app.listen(PORT || 3000);
}

bootstrap().catch(e => {
  Logger.error(`❌ Error starting server, ${e}`, "", "Bootstrap", false);
  throw e;
});
