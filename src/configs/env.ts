import * as dotenv from "dotenv";

dotenv.config();

// environment
export const NODE_ENV: string = process.env.NODE_ENV || "development";

// application
export const DOMAIN: string = process.env.DOMAIN || "localhost";
export const PORT: number = +process.env.PORT || 3000;
export const END_POINT: string = process.env.END_POINT || "graphql";
export const CLIENT_URL: string = process.env.CLIENT_URL || "xxx";
export const RATE_LIMIT_MAX: number = +process.env.RATE_LIMIT_MAX || 10000;
export const DEFAULT_ACCOUNT_USERNAME: string = process.env.DEFAULT_ACCOUNT_USERNAME || "admin";
export const DEFAULT_ACCOUNT_EMAIL: string = process.env.DEFAULT_ACCOUNT_EMAIL || "admin@northstudio.vn";
export const DEFAULT_ACCOUNT_PASSWORD: string = process.env.DEFAULT_ACCOUNT_PASSWORD || "admin@123";

// static
export const STATIC: string = process.env.STATIC || "static";

// mongodb
export const MONGO_URI = process.env.MONGO_URI || "mongodb://localhost/squadev";

// jsonwebtoken
export const ISSUER: string = process.env.ISSUER || "nestjs_base";
export const ACCESS_TOKEN_SECRET: string = process.env.ACCESS_TOKEN_SECRET || "access-token-key";
export const ACCESS_TOKEN_EXPIRES_TIME: string = process.env.ACCESS_TOKEN_EXPIRES_TIME || "30s";

export const REFRESH_TOKEN_SECRET: string = process.env.REFRESH_TOKEN_SECRET || "refresh-token-key";
export const REFRESH_TOKEN_EXPIRES_TIME: string = process.env.REFRESH_TOKEN_EXPIRES_TIME || "14d";

export const EMAIL_TOKEN_SECRET: string = process.env.EMAIL_TOKEN_SECRET || REFRESH_TOKEN_SECRET;
export const EMAIL_VERIFY_TOKEN_EXPIRES_TIME: string = process.env.EMAIL_VERIFY_TOKEN_EXPIRES_TIME || "1d";

export const RESET_PASS_TOKEN_SECRET: string = process.env.RESETPASS_TOKEN_SECRET || REFRESH_TOKEN_SECRET;
export const RESET_PASS_TOKEN_EXPIRES_TIME: string = process.env.RESET_PASS_TOKEN_EXPIRES_TIME || "10m";

// bcrypt
export const BCRYPT_SALT: number = +process.env.BCRYPT_SALT || 10;

// Google services
export const GOOGLE_API_BASE_URL: string = process.env.GOOGLE_API_BASE_URL || "https://www.googleapis.com";
export const GOOGLE_CLIENT_ID: string = process.env.GOOGLE_CLIENT_ID || "xxx";
export const GOOGLE_CLIENT_SECRET: string = process.env.GOOGLE_CLIENT_SECRET || "xxx";
export const GOOGLE_REDIRECT_URI: string = process.env.GOOGLE_REDIRECT_URI || "auth/google/callback";

// Facebook services
export const FACEBOOK_CLIENT_ID: string = process.env.FACEBOOK_APP_ID || "xxx";
export const FACEBOOK_CLIENT_SECRET: string = process.env.FACEBOOK_APP_SECRET || "xxx";
export const FACEBOOK_REDIRECT_URI: string = process.env.FACEBOOK_REDIRECT_URI || "auth/facebook/callback";

// Gitlab services
export const GITLAB_BASE_URL: string = process.env.GITLAB_BASE_URL || "https://gitlab.com";
export const GITLAB_API_BASE_URL: string = process.env.GITLAB_BASE_URL || "https://gitlab.com/api/v4";
export const GITLAB_CLIENT_ID: string = process.env.GITLAB_CLIENT_ID || "xxx";
export const GITLAB_CLIENT_SECRET: string = process.env.GITLAB_CLIENT_SECRET || "xxx";
export const GITLAB_REDIRECT_URI: string = process.env.GITLAB_REDIRECT_URI || "http://localhost:3000/oauth/gitlab";
export const GITLAB_SCOPE: string = process.env.GITLAB_SCOPE || "api+write_repository";

// mailgun
export const MAILGUN_API_KEY = process.env.MAILGUN_API_KEY || "xx";
export const MAILGUN_DOMAIN = process.env.MAILGUN_DOMAIN || "xx";

// email
export const EMAIL_FROM: string = `Squadev <no-reply@squadev.northstudio.vn>`;
export const EMAIL_VERIFY_ACCOUNT_URL: string = process.env.EMAIL_VERIFY_ACCOUNT_URL || `/verify/email/check`;
export const EMAIL_RESET_PASSWORD_URL: string = process.env.EMAIL_RESET_PASSWORD_URL || `/reset-password/check`;

// google cloud
export const GCLOUD_BUCKET_ID: string = process.env.GCLOUD_BUCKET_ID || "northstudio-internal";
export const GCLOUD_PROJECT_ID: string = process.env.GCLOUD_PROJECT_ID || "projectId";
export const GCLOUD_CLIENT_EMAIL: string = process.env.GCLOUD_CLIENT_EMAIL || "clientEmail";
export const GCLOUD_PRIVATE_KEY: string = process.env.GCLOUD_PRIVATE_KEY && process.env.GCLOUD_PRIVATE_KEY.replace(/(\|\|)/g, "\n");

// reCAPTCHA
export const RECAPTCHA_SECRET: string = process.env.RECAPTCHA_SECRET || "reCAPTCHA_secret";
