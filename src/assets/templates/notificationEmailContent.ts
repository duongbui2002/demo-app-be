const notificationEmailContent = (content, title) => `
<html>
<head>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
</head>
<body>
  <div
    style="
      font-family: 'Roboto', sans-serif;
      padding: 30px;
      margin: 0 auto;
      border-radius: 10px;
      width: 500px;
      text-align: center;
      box-shadow: 3px 4px 4px 4px rgba(0, 0, 0, 0.1);
    "
  >
    <div>
      <div>
        <img  src="https://cdn.pixabay.com/photo/2016/12/26/18/33/logo-1932539__340.png"
            height="50"
            width="50"
          />
      </div>
      <h2  style="text-align: center; margin-top: 16px">
        ${content}
      </h2>
      <p style="
        text-align: center;
      "> ${title}</p>
        <button style="
        margin: 0;
        background-color: #008CBA;
        border-radius: 8px;
        border: none;
        color: white;
        padding: 8px 20px;
        text-align: center;
        text-decoration: none;
        font-size: 14px;
      ">View link</button>
    </div>
  </div>
</body>
</html>
`;

// TODO: Add link to stable task management
export default notificationEmailContent
