const emailTemplates = {
  verifyEmail: (clientVerifyUrl: string, verifyToken: string) => {
    return generateTemplate({
      title: "Let's verify account",
      redirectUrl: `${clientVerifyUrl}?verify_token=${verifyToken}`,
      content: `Please verify your email address to complete your Squadev account:`,
      warning: "This link will expire in 24 hours. If it has expired, try to request a new verification email.",
      verifyButton: "Verify account"
    });
  },
  changePassword: (clientVerifyUrl: string, verifyToken: string, username: string) => {
    return generateTemplate({
      title: "Let's change password",
      redirectUrl: `${clientVerifyUrl}?verify_token=${verifyToken}`,
      content: `Welcome to MonokaiToolkit Forum, You can sign in using this username: <b>${username}</b>.\nClick on the link below to change your password!`,
      warning: "This link will expire in 10 minutes. If it has expired, try to request a new verification email.",
      verifyButton: "Change password"
    });
  },

};

function generateTemplate(config: { title: string, redirectUrl: string, content: string, warning?: string, verifyButton: string }) {
  return `<html>
  <head>
    <style>
      body {
        font-family: sans-serif;
      }
      .controls-section {
        text-align: center;
      }
    </style>
  </head>
  <body>
    <div style="background: #f2f6ff; padding: 24px 0">
      <div style="margin-left: auto;margin-right: auto;margin-bottom: 24px;width: 56px;">
        <img
          src="https://storage.googleapis.com/northstudio-internal/media-monokai.pngC87PPay8ZT-1669019217091.png"
          height="56"
          width="56"
        />
      </div>
      <div
        style="
          padding: 12px 24px;
          border-radius: 10px;
          border: 1px solid rgba(0, 0, 0, 0.1);
          width: 530px;
          margin: 0 auto;
          box-shadow: 3px 4px 4px rgba(0, 0, 0, 0.05);
          background-color: #fff;
          text-align: justify;
        "
      >
        <h1 style="text-align: center; color: rgb(17, 5, 63)">
          ${config.title}
        </h1>
        <p
          style="
            margin-top: 4px;
            font-weight: 400;
            font-size: 18px;
            margin-bottom: 6px;
            color: #333;
          "
        >
         ${config.content}
        </p>
        <p style="font-size: 15px; margin-top: 0; color: #333">
          ${config.warning ? config.warning : ""}
        </p>
        <div class="controls-section">
          <a href="${config.redirectUrl}">
            <button
              id="button-link"
              style="
                background-color: #3e79f7;
                color: white;
                padding: 10px 20px;
                border-radius: 10px;
                cursor: pointer;
                border: none !important;
                margin-bottom: 20px;
                margin-top: 12px;
                transition: all 0.25s;
                font-size: 15px;
                font-family: sans-serif;
              "
              onMouseOver="this.style.opacity='0.8'"
              onMouseOut="this.style.opacity='1'"
            >
              ${config.verifyButton}
            </button>
          </a>
        </div>
      </div>
      <div style="width: fit-content; margin: 0 auto">
        <div
          style="
            font-weight: 700;
            margin-top: 24px;
            vertical-align: middle;
            position: relative;
            text-align: center;
          "
        >
          <div
            style="display: inline-block; vertical-align: middle; height: 35px"
          >
            <img
              src="https://storage.googleapis.com/northstudio-internal/media-monokai.pngC87PPay8ZT-1669019217091.png"
              height="24"
              width="24"
              style="
                margin-right: 8px;
                display: block;
                position: absolute;
                top: 50%;
                transform: translateY(-50%);
              "
            />
          </div>
          <div
            style="
              color: rgb(17, 5, 63);
              display: inline-block;
              line-height: 35px;
            "
          >
            Squadev
          </div>
        </div>
        <div style="font-size: 13px; margin-bottom: 24px; margin-top: 4px">
          You're receiving this email from Squadev.
        </div>
      </div>
    </div>
  </body>
</html>`;
}

export default emailTemplates;
