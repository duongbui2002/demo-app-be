import { CanActivate, ExecutionContext, mixin, Type } from "@nestjs/common";

import { RoleEnum } from "@modules/auth/auth.enum";

import { AuthGuard } from "@common/guards/auth.guard";


const RoleGuard = (role?: RoleEnum): Type<CanActivate> => {
  class RoleGuardMixin extends AuthGuard {
    async canActivate(context: ExecutionContext) {
      await super.canActivate(context);

      const request = context.switchToHttp().getRequest();
      const account = request.account;

      return !role || role && account?.roles.includes(role);
    }
  }

  return mixin(RoleGuardMixin);
};

export default RoleGuard;
