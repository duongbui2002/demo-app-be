import { CanActivate, ExecutionContext, HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { AuthService } from "@modules/auth/auth.service";
import { getTokenFromRequest } from "@src/utils/get-token-from-request";


@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private authService: AuthService
  ) {
  }

  async canActivate(
    context: ExecutionContext
  ) {

    const ctx = context.switchToHttp();
    const req = ctx.getRequest();
    const token = getTokenFromRequest(req);

    if (!token) {
      throw new HttpException("Unauthorized", HttpStatus.UNAUTHORIZED);
    }

    const tokenRes = await this.authService.verifyTokenFromRequest(token);

    req.account = tokenRes.account;

    return true;
  }
}
