import { Global, Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { AccountSchema } from "@modules/accounts/account.schema";
import { DefaultAccount } from "@src/utils/default-account";
import { TokenSchema } from "@modules/tokens/token.schema";
import { TaskSchema } from '@modules/task/task.schema';

const mongooseModulesForFeature = [{
  name: "Account", schema: AccountSchema
}, {
  name: "Token", schema: TokenSchema
}, {
  name: "Task", schema: TaskSchema
}];

@Global()
@Module({
  imports: [
    MongooseModule.forFeature(mongooseModulesForFeature),
    MongooseModule.forFeatureAsync([{
      name: "Account",
      useFactory: () => {
        const schema = AccountSchema;
        const accountModel = new DefaultAccount(schema);
        accountModel.generate().then(_ => {
        });
        return schema;
      }
    }])
  ],

  exports: [
    MongooseModule.forFeature(mongooseModulesForFeature)
  ]
})
export class DatabaseModule {
}
