import { diskStorage } from "multer";
import { MulterOptions } from "@nestjs/platform-express/multer/interfaces/multer-options.interface";
import { HttpException, HttpStatus } from "@nestjs/common";
import { extension } from "mime-types";

export class UploadService {
  static getMulterConfig(config?: { dest?: string, fileSizeLimit?: number, allowedExt?: string[] }): MulterOptions {
    return {
      dest: config.dest,
      storage: diskStorage({
        destination: function(req, file, cb) {
          cb(null, config.dest);
        },
        filename: function(req, file, cb) {
          const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1E9);
          cb(null, uniqueSuffix + "-" + file.originalname);
        }
      }),
      fileFilter: async function(req, file, cb) {
        const fileSize = parseInt(req.headers["content-length"]) - 350;

        const fileExt = await extension(file.mimetype);

        if (config) {
          if (config.fileSizeLimit && config.fileSizeLimit < fileSize) return cb(new HttpException("This file is too large", HttpStatus.BAD_REQUEST), false);

          if (fileExt && config.allowedExt && !config.allowedExt.includes(fileExt)) return cb(new HttpException(`This file extension is not allowed`, HttpStatus.BAD_REQUEST), false);
        }

        cb(null, true);

      }
    };
  }
}