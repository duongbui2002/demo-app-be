import * as mongoose from "mongoose";
import {
  Aggregate,
  ClientSession,
  Document,
  FilterQuery,
  HydratedDocument,
  PaginateModel,
  PaginateOptions,
  PipelineStage,
  QueryOptions,
  QueryWithHelpers,
  SaveOption,
  SaveOptions,
  UnpackedIntersection,
  UpdateQuery
} from "mongoose";
import { HttpException } from "@nestjs/common";
import { handleAggregateOptions, paginateAggregate } from "@src/utils/paginate-aggregate";
import { PaginatedDocumentsResponseDto } from "@common/dto/pagination-params.dto";

export class BaseService<T extends Document> {
  constructor(private model: PaginateModel<T>) {
  }

  async create(createDocumentDto: any, options?: SaveOption): Promise<HydratedDocument<T, {}, {}>> {
    const document = await this.model.create(createDocumentDto);
    if (options?.populate) await document.populate(options?.populate);
    return document;
  }

  async createMany(createDocumentDto: any[], options?: SaveOptions): Promise<HydratedDocument<T, {}, {}>[]> {
    const documents = await this.model.create(createDocumentDto, options);
    if (options?.populate) {
      let arr = [];
      documents.forEach(document => arr.push(document.populate(options.populate)));
      await Promise.all(arr);
    }

    return documents;
  }

  async findAll(filter: FilterQuery<T>, options?: PaginateOptions): Promise<PaginatedDocumentsResponseDto<T>> {
    const paginateResult = await this.model.paginate(filter, options);
    const data = paginateResult.docs;
    delete paginateResult.docs;
    return {
      data,
      paginationOptions: paginateResult
    };
  }

  async aggregate(filter: FilterQuery<T>, pipeline: PipelineStage[] = [], options: PaginateOptions = { pagination: false }): Promise<Aggregate<Array<T> | PaginatedDocumentsResponseDto<T>> | any> {
    Object.keys(filter).forEach(data => {
      if (mongoose.isValidObjectId(filter[data])) Object.assign(filter, { [data]: new mongoose.Types.ObjectId(filter[data]) });
    });

    if (!options.limit) options.limit = 10;
    if (!options.page) options.page = 1;
    options.sort = options["sort"] ? (options["sort"] as string) : "-_id";

    const paginationOptions = (options.pagination === true) ? paginateAggregate(options) : handleAggregateOptions(options);
    const results = await this.model.aggregate([{
        $match: filter
      },
        ...pipeline, ...paginationOptions
      ]
    );

    if (options?.pagination === true) {
      return results.length > 0 ? results[0] : {
        data: [],
        paginationOptions: {
          totalDocs: 0,
          limit: options.limit,
          totalPages: 1,
          page: options.page,
          pagingCounter: 1,
          hasPrevPage: false,
          hasNextPage: false,
          prevPage: null,
          nextPage: null
        }
      };
    } else
      return results;
  }


  async findOne(filter: FilterQuery<T>, options?: QueryOptions): Promise<UnpackedIntersection<HydratedDocument<T, {}, {}>, {}>> {
    const document = await this.model.findOne(filter, null, options).populate(options?.populate).select(options?.select);
    if (options?.nullable !== true && !document) throw new HttpException(`${this.model.modelName} not found`, 500);

    return document;
  }

  async update(filter: FilterQuery<T>, updateDocumentDto: UpdateQuery<T>, options?: QueryOptions): Promise<HydratedDocument<T, {}, {}>> {
    const document = await this.model.findOneAndUpdate(filter, updateDocumentDto, options);
    if (options?.nullable !== true && !document) throw new HttpException(`${this.model.modelName} not found`, 500);
    if (options?.populate) await document.populate(options?.populate);
    return document;
  }

  async updateAll(filter: FilterQuery<T>, updateDocumentDto: UpdateQuery<T>, options?: QueryOptions): Promise<mongoose.mongo.UpdateResult> {
    return this.model.updateMany(filter, updateDocumentDto, options);
  }

  async remove(filter: FilterQuery<T>, options?: QueryOptions): Promise<UnpackedIntersection<HydratedDocument<T, {}, {}>, {}>> {
    const document = await this.model.findOneAndDelete(filter, options).populate(options?.populate);
    if (options?.nullable !== true && !document) throw new HttpException(`${this.model.modelName} not found`, 500);

    return document;
  }

  async removeAll(filter: FilterQuery<T>, options?: QueryOptions) {
    return this.model.deleteMany(filter, options).populate(options?.populate);
  }

  count(filter: FilterQuery<T>): QueryWithHelpers<number, HydratedDocument<T, {}, {}>, {}, T> {
    return this.model.countDocuments(filter);
  }

  // execute commands and database query with transaction
  async withTx(cb: (session: ClientSession) => any) {
    const session = await this.model.startSession();

    session.startTransaction();

    try {
      const res = await cb(session);

      await session.commitTransaction();
      await session.endSession();

      return res;

    } catch (err) {
      await session.abortTransaction();
      await session.endSession();

      throw err;
    }
  }
}
