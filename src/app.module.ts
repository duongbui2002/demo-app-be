import { MONGO_URI } from "@configs/env";
import { AppController } from "@src/app.controller";
import { Module } from "@nestjs/common";
import { APP_FILTER, RouterModule } from "@nestjs/core";
import { MongooseModule } from "@nestjs/mongoose";
import { AccountsModule } from "@modules/accounts/accounts.module";
import { DatabaseModule } from "@common/database/database.module";
import { AllExceptionsFilter } from "@common/filters/all-exceptions.filter";
import { AuthModule } from "@modules/auth/auth.module";
import { TokensModule } from "@modules/tokens/tokens.module";
import { TaskModule } from './modules/task/task.module';


@Module({
  imports: [
    MongooseModule.forRoot(MONGO_URI),
    DatabaseModule,
    AccountsModule,
    AuthModule,
    TokensModule,
    TaskModule,
  ],
  controllers: [
    AppController
  ],
  providers: [{
    provide: APP_FILTER,
    useClass: AllExceptionsFilter
  }]
})
export class AppModule {
}
