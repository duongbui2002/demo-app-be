import { IsEmail, IsMongoId, IsNotEmpty, IsOptional, IsString, MaxLength, MinLength } from "class-validator";

export class LoginDto {

  @IsOptional()
  @IsMongoId()
  team: string;

  @IsString()
  @IsNotEmpty()
  loginField: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(8)
  @MaxLength(32)
  password: string;

}
