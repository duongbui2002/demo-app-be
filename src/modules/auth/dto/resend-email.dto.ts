import { IsEmail, IsNotEmpty, IsString, MaxLength, MinLength } from "class-validator";

export class ResendEmailDto {
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @IsString()
  recaptcha: string;

  remoteAddress: string;
}
