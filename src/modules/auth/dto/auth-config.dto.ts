export class SSOConfig {
  grantType: string;
  redirectUri: string;
  authorizationCode: string;
}

export class AuthConfig {
  grantType: string;
  redirectUri?: string;
  clientID: string;
  clientSecret: string;
}

export class AuthData {
  grantType?: string;
  clientID?: string;
  clientSecret?: string;
  refreshToken?: string;
  code?: string;
}

export class AuthTokenPayload {
  accountId: string;
  teamId?: string;
}

export class TokenPayload {
  accountId: string;
  type: import("@modules/tokens/token.enum").TokenType;
}