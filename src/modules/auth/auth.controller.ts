import { Body, Controller, HttpException, HttpStatus, Post, Req, Res, UseGuards } from "@nestjs/common";
import { AuthService } from "@modules/auth/auth.service";
import { AccountsService } from "@modules/accounts/accounts.service";
import { RegisterAccountDto } from "@modules/auth/dto/register-account.dto";
import { LoginDto } from "@modules/auth/dto/login.dto";
import { LoginWithSsoDto } from "@modules/auth/dto/login-with-sso.dto";
import { TokensService } from "@modules/tokens/tokens.service";
import { AuthGuard } from "@common/guards/auth.guard";
import { ResendEmailDto } from "@modules/auth/dto/resend-email.dto";
import { LogoutDto } from "@modules/auth/dto/logout.dto";
import { AccountDecorator } from "@common/decorators/account.decorator";
import { Request } from "express";
import { AccountDocument } from "@modules/accounts/account.schema";
import TokenEnum from "@modules/tokens/token.enum";
import { ACCESS_TOKEN_EXPIRES_TIME } from "@configs/env";

@Controller("auth")
export class AuthController {
  constructor(
    private authService: AuthService,
    private accountService: AccountsService,
    private tokenService: TokensService
  ) {
  }
  @Post("register")
  async register(
    @Req() req: Request,
    @Body() registerAccountDto: RegisterAccountDto
  ) {
    await this.authService.register(registerAccountDto);
    return {
      message: "Register account successfully, please verify your email before signing in",
      success: true
    };
  }


  @Post("forget_password")
  async forgetPassword(
    @Req() req: Request,
    @Body() forgetPasswordDto: ResendEmailDto
  ) {
    const account = await this.accountService.findOne({ email: forgetPasswordDto.email }, { nullable: true });

    forgetPasswordDto.remoteAddress = req.socket.remoteAddress;


    if (account) await this.authService.generateVerifyTokenAndSendEmail(TokenEnum.RESET_PASS, account, forgetPasswordDto.recaptcha, forgetPasswordDto.remoteAddress);

    return {
      message: "A confirmation email was sent, please check your inbox to reset your password."
    };
  }

  @Post("login")
  async login(@Body() loginDto: LoginDto) {
    try {

      const account = await this.authService.authenticate(loginDto.loginField, loginDto.password);
      if (!account) {
        throw new HttpException("Login failed", HttpStatus.NOT_FOUND);
      }

      const payload = {
        accountId: account._id
      };

      let {
        access_token,
        refresh_token,
        accessTokenExpiresIn
      } = await this.tokenService.generateAuthTokens(payload, account);

      return {
        access_token,
        access_token_expires_in: accessTokenExpiresIn,
        refresh_token,
        account,
        success: true
      };
    } catch (e) {
      console.log(e);
      if (e.message === "verifyAccount") throw new HttpException("Please verify your email to continue", HttpStatus.INTERNAL_SERVER_ERROR);
      else throw new HttpException("Login failed", HttpStatus.UNAUTHORIZED);
    }
  }

  @Post("refresh")
  async refreshToken(@Req() req, @Res() res) {

    let refresh_token;
    if (req.body.refresh_token) {
      refresh_token = await this.tokenService.findOne({ token: req.body.refresh_token });
      if (!refresh_token) {
        throw new HttpException("Unauthorized", HttpStatus.UNAUTHORIZED);
      }
    }

    const payload = await this.tokenService.verifyToken(refresh_token.token, TokenEnum.REFRESH);

    const access_token = this.tokenService.generateToken({
      accountId: payload.accountId,
      type: TokenEnum.ACCESS
    }, TokenEnum.ACCESS, { expiresIn: ACCESS_TOKEN_EXPIRES_TIME });

    const accessTokenExpiresIn = await this.tokenService.generateTokensExpirationTime(TokenEnum.ACCESS);

    const account = await this.accountService.findOne({ _id: payload.accountId });
    if (!account) {
      throw  new HttpException("Account does not exist", HttpStatus.NOT_FOUND);
    }
    return res.status(HttpStatus.OK).json({
      access_token, access_token_expires_in: accessTokenExpiresIn, account, success: true
    });
  }

  @Post("logout")
  @UseGuards(AuthGuard)
  async logout(@Body() logoutDto: LogoutDto, @AccountDecorator() account: AccountDocument) {

    await this.tokenService.deleteOne({
      author: account._id,
      token: logoutDto.refreshToken
    });

    return {
      message: "Log out successfully"
    };
  }

}
