import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { AccountsService } from '@modules/accounts/accounts.service';
import { compareSync } from 'bcryptjs';
import { RegisterAccountDto } from '@modules/auth/dto/register-account.dto';
import { TokensService } from '@modules/tokens/tokens.service';
import TokenEnum from '@modules/tokens/token.enum';
import { AccountDocument } from '@modules/accounts/account.schema';

@Injectable()
export class AuthService {
  supportedServicesOAuth: string[] = ["google", "facebook"];

  constructor(
    private accountService: AccountsService,
    private tokenService: TokensService,
  ) {
  }

  async register(registerAccountDto: RegisterAccountDto) {
    if (await this.accountService.count({ $or: [{ username: registerAccountDto.username }, { email: registerAccountDto.email }] }) > 0) throw new HttpException("This account is already exists", 500);
    const account = await this.accountService.createAccountInstance(registerAccountDto);

    account.isActivated = true;
    await account.save();
    return account;
  }

  async authenticate(loginField: string, password: string) {
    let account = await this.accountService.findOne({ $or: [{ email: loginField }, { username: loginField }] }, {
      select: "username password email isActivated"
    });

    if (!account.isActivated) {
      throw new HttpException("verifyAccount", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    const check = await this.comparePassword(password, account.password);
    if (!account || !check) {
      throw new HttpException("Failed to login", HttpStatus.UNAUTHORIZED);
    }

    account = await this.accountService.findOne({ _id: account._id });
    return account;
  }


  async generateVerifyTokenAndSendEmail(type: TokenEnum.EMAIL_VERIFY | TokenEnum.RESET_PASS, account, recaptcha: string, remoteAddress: string) {
    if (await this.tokenService.count({ account: account._id, type }) <= 0) {
      const verifyTokenExpiresIn = await this.tokenService.generateTokensExpirationTime(type);

      const tokenExpiresTime = this.tokenService.generateTokensExpirationTime(type);

      const emailToken = this.tokenService.generateToken({
        accountId: account._id,
        type
      }, type, { expiresIn: tokenExpiresTime });

      await this.tokenService.saveToken(emailToken, account, type, verifyTokenExpiresIn);

    }
  }

  async verifyTokenFromRequest(token: string): Promise<{ account: AccountDocument }> {
    let payload;
    try {
      payload = this.tokenService.verifyToken(token, TokenEnum.ACCESS);
    } catch (e) {
      Logger.error(e);
      throw new HttpException("Unauthorized", HttpStatus.UNAUTHORIZED);
    }

    const account = await this.accountService.findOne({ _id: payload.accountId }, { nullable: true });

    if (!account) {
      throw new HttpException("Account does not exist", HttpStatus.UNAUTHORIZED);
    }

    return { account };

  }

  async comparePassword(password: string, storePasswordHash: string): Promise<boolean> {
    return compareSync(password, storePasswordHash);
  }

}
