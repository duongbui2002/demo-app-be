import { IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { TaskEnum } from '@modules/task/task.schema';

export class UpdateTaskDto {
  @IsOptional()
  @IsString()
  title: string;

  @IsOptional()
  @IsString()
  description: string ;

  @IsEnum(TaskEnum)
  @IsOptional()
  status: TaskEnum = TaskEnum.TO_DO;
}
