import { IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { TaskEnum } from '@modules/task/task.schema';

export class CreateTaskDto {
  @IsNotEmpty()
  @IsString()
  title: string;

  @IsNotEmpty()
  @IsString()
  description: string;

  @IsEnum(TaskEnum)
  @IsOptional()
  status: TaskEnum = TaskEnum.TO_DO;
}
