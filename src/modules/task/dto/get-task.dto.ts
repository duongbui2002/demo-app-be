import { IsOptional, IsString } from 'class-validator';

export class GetTaskDto{
  @IsOptional()
  @IsString()
  title: string;
}