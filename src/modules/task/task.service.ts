import { Injectable } from '@nestjs/common';
import { BaseService } from '@common/services/base.service';
import { TaskDocument } from '@modules/task/task.schema';
import { InjectModel } from '@nestjs/mongoose';
import {   PaginateModel} from 'mongoose';

@Injectable()
export class TaskService extends BaseService<TaskDocument>{
  constructor(@InjectModel("Task") private readonly taskModel: PaginateModel<TaskDocument>) {
   super(taskModel);
  }

}
