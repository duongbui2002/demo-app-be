import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards, UseInterceptors } from '@nestjs/common';
import { TaskService } from '@modules/task/task.service';
import { CreateTaskDto } from '@modules/task/dto/create-task.dto';
import { TransformInterceptor } from '@common/intercepters/transform.intercepter';
import { AuthGuard } from '@common/guards/auth.guard';
import { AccountDocument } from '@modules/accounts/account.schema';
import { AccountDecorator } from '@common/decorators/account.decorator';
import { PaginationParamsDto } from '@common/dto/pagination-params.dto';
import { UpdateTaskDto } from '@modules/task/dto/update-task.dto';

@Controller('task')
@UseInterceptors(TransformInterceptor)
export class TaskController {
  constructor(private readonly taskService: TaskService) {
  }

  @Post()
  @UseGuards(AuthGuard)
  async createTask(@Body() createTaskDto: CreateTaskDto, @AccountDecorator() account: AccountDocument) {
    return await this.taskService.create({
      ...createTaskDto,
      owner: account._id,
    });
  }

  @Get()
  @UseGuards(AuthGuard)
  async getTask(@AccountDecorator() account: AccountDocument, @Query() pagination: PaginationParamsDto) {
    return await this.taskService.findAll({
      owner: account._id,
    }, {
      ...pagination,
    });
  }

  @UseGuards(AuthGuard)
  @Get(':id')
  async getTaskById(@AccountDecorator() account: AccountDocument, @Param('id') id: string) {
    return await this.taskService.findOne({
      owner: account._id,
      id: id,
    });
  }

  @Put(':id')
  @UseGuards(AuthGuard)
  async updateTask(@AccountDecorator() account: AccountDocument, @Param('id') id: string, @Body() updateTaskDto: UpdateTaskDto) {
    for (const key in updateTaskDto) {
      if (!updateTaskDto[key]) {
        delete updateTaskDto[key];
      }
    }
    return await this.taskService.update({
      owner: account._id,
      id: id,
    }, {
      ...updateTaskDto,
    });
  }

  @UseGuards(AuthGuard)
  @Delete(':id')
  async deleteTask(@AccountDecorator() account: AccountDocument, @Param('id') id: string) {
    await this.taskService.remove({ _id: id, owner: account._id });
    return`Delete task #${id} successfully`;
  }
}
