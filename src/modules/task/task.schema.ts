import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";
import * as paginate from "mongoose-paginate-v2";
import * as mongoose from "mongoose";
import { Account } from "@modules/accounts/account.schema";


export type TaskDocument = Task & Document;
export enum TaskEnum {
  INCOMPLETE = "incomplete",
  COMPLETE = "complete",
  CANCELLED = "cancelled",
  IN_PROGRESS = "in_progress",
  TO_DO = "to_do"
}
@Schema({
  timestamps: true
})
export class Task {

  @Prop({
    required: true,
    type: mongoose.Schema.Types.ObjectId, ref: "Account"
  })
  owner: Account;

  @Prop({

    index: true,
    trim: true
  })
  title: string;

  @Prop({
  })
  description: string;

  @Prop()
  status: TaskEnum;
}

export const TaskSchema = SchemaFactory.createForClass(Task);

TaskSchema.plugin(paginate);
