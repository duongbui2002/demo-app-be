import { Injectable } from "@nestjs/common";
import { AccountDocument } from "@modules/accounts/account.schema";
import {
  FilterQuery,
  HydratedDocument,
  PaginateModel,
  QueryOptions,
  UnpackedIntersection,
  UpdateQuery
} from "mongoose";
import { InjectModel } from "@nestjs/mongoose";
import { BaseService } from "@common/services/base.service";
import { UpdateAccountDto } from "@modules/accounts/dto/update-account.dto";

@Injectable()
export class AccountsService extends BaseService<AccountDocument> {
  constructor(@InjectModel("Account") private accountModel: PaginateModel<AccountDocument>) {
    super(accountModel);
  }

  async createAccountInstance(account: Partial<AccountDocument>) {
    return new this.accountModel({
      email: account.email,
      username: account.username,
      password: account.password
    });
  }

  async create(account: Partial<AccountDocument>) {
    return await this.accountModel.create({
      email: account.email,
      username: account.username,
      password: account.password
    });
  }

  async update(filter: FilterQuery<AccountDocument>, updateDocumentDto: UpdateAccountDto & UpdateQuery<AccountDocument>, options?: QueryOptions): Promise<UnpackedIntersection<HydratedDocument<AccountDocument, {}, {}>, {}>> {
    if (updateDocumentDto.roles) Object.assign(updateDocumentDto,  { roles: updateDocumentDto.roles } )
    else if (updateDocumentDto.role) Object.assign(updateDocumentDto, { $addToSet: { roles: updateDocumentDto.role } })
    else if (updateDocumentDto.removeRole) Object.assign(updateDocumentDto, { $pull: { roles: updateDocumentDto.removeRole } });

    return super.update(filter, updateDocumentDto, options);
  }

}
