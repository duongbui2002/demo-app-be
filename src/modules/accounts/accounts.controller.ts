import {
  Body,
  Controller,
  FileTypeValidator,
  Get,
  HttpException,
  HttpStatus,
  MaxFileSizeValidator,
  Param,
  ParseFilePipe,
  Patch,
  Post,
  Query,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { AccountsService } from './accounts.service';
import { UpdateAccountDto, UpdateSelfAccountDto } from './dto/update-account.dto';
import { RoleEnum } from '@modules/auth/auth.enum';
import RoleGuard from '@common/guards/roles.guard';
import { AuthGuard } from '@common/guards/auth.guard';
import { ChangePasswordDto } from '@modules/accounts/dto/change-password.dto';
import { AuthService } from '@modules/auth/auth.service';
import { PaginationParamsDto } from '@common/dto/pagination-params.dto';
import { AccountDecorator } from '@common/decorators/account.decorator';
import { AccountDocument } from '@modules/accounts/account.schema';
import { TransformInterceptor } from '@common/intercepters/transform.intercepter';
import { FileInterceptor } from '@nestjs/platform-express';
import { Express } from 'express';
import { randomString } from '@src/utils/random';
import { PublicGuard } from '@common/guards/public.guard';
import { FindAccountFilterDto } from '@modules/accounts/dto/find-account-filter.dto';
import { PublicQuerySelection } from '@modules/accounts/account.enum';

@Controller("accounts")
@UseInterceptors(TransformInterceptor)
export class AccountsController {
  constructor(
    private readonly accountsService: AccountsService,
    private readonly authService: AuthService,
  ) {
  }

  @Get("info")
  @UseGuards(AuthGuard)
  async getInfo(@AccountDecorator() account: AccountDocument) {
    return account;
  }

  @Get("public")
  @UseGuards(AuthGuard)
  async getPublicAccounts(
    @Query() filter: FindAccountFilterDto,
    @Query() options: PaginationParamsDto
  ) {
    Object.assign(options, { select: "avatar displayName roles" });
    return this.accountsService.findAll(filter, options);
  }

  @Get("public/:id")
  @UseGuards(PublicGuard)
  async getPublicAccount(
    @AccountDecorator() account: AccountDocument,
    @Param("id") accountId,
    @Query() options: PaginationParamsDto
  ) {
    return this.accountsService.findOne({ _id: accountId }, (account && account.roles.includes(RoleEnum.ADMIN)) ? {} : { select: PublicQuerySelection });
  }

  @Get()
  @UseGuards(RoleGuard(RoleEnum.ADMIN))
  getAccounts(
    @Query() filter: FindAccountFilterDto,
    @Query() options: PaginationParamsDto
  ) {
    return this.accountsService.findAll(filter, options);
  }

  @Post("change_password")
  @UseGuards(AuthGuard)
  async changePassword(
    @AccountDecorator() account: AccountDocument,
    @Body() changePasswordDto: ChangePasswordDto
  ) {
    if (changePasswordDto.currentPassword === changePasswordDto.newPassword) {
      throw new HttpException("Old and new passwords must be different", HttpStatus.BAD_REQUEST);
    }

    const { password } = await this.accountsService.findOne({ _id: account._id }, { select: "password" });

    if (password && !(await this.authService.comparePassword(changePasswordDto.currentPassword, password))) {
      throw new HttpException("Current password is incorrect", HttpStatus.BAD_REQUEST);
    }

    await this.accountsService.update({ _id: account._id }, { password: changePasswordDto.newPassword }, { new: true });

    return {
      message: "Changed password successfully"
    };
  }



  @Post("upload_avatar")
  @UseGuards(AuthGuard)
  @UseInterceptors(FileInterceptor("avatar"))
  async uploadAvatar(
    @AccountDecorator() account: AccountDocument,
    @UploadedFile(
      new ParseFilePipe({
        validators: [
          new MaxFileSizeValidator({ maxSize: 2000 * 1000 }),
          new FileTypeValidator({ fileType: new RegExp("([^\\\\s]+(jpe?g|png|gif|bmp)$)") })
        ]
      })
    ) file: Express.Multer.File
  ) {
    let avatar = `${file.fieldname}-${randomString(account.username, 10)}-${new Date().getTime()}.jpg`;
    // avatar = await this.googleStorageService.upload(avatar, file.buffer);

    const updatedAccount = await this.accountsService.update({ _id: account._id }, { avatar }, { new: true });

    return {
      data: updatedAccount,
      message: "Uploaded avatar successfully."
    };
  }

  @Post(":id/activate")
  @UseGuards(RoleGuard(RoleEnum.ADMIN))
  activeAccount(
    @Param("id") accountId: string
  ) {
    if (!accountId) {
      throw new HttpException("Account id is not provided", HttpStatus.BAD_REQUEST);
    }
    return this.accountsService.update({ _id: accountId }, { isActivated: true }, { new: true });
  }

  @Patch("me")
  @UseGuards(AuthGuard)
  updateSelf(
    @AccountDecorator() account: AccountDocument,
    @Body() updateAccountDto: UpdateSelfAccountDto
  ) {

    return this.accountsService.update({ _id: account._id }, updateAccountDto, { new: true });
  }

  @Patch(":id")
  @UseGuards(RoleGuard(RoleEnum.ADMIN))
  update(
    @Param("id") id: string,
    @Body() updateAccountDto: UpdateAccountDto
  ) {
    if (updateAccountDto.role) Object.assign(updateAccountDto, { $addToSet: { roles: updateAccountDto.role } });
    return this.accountsService.update({ _id: id }, updateAccountDto, { new: true });
  }
}
