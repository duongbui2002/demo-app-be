import { Global, Module } from "@nestjs/common";
import { AccountsService } from "./accounts.service";
import { AccountsController } from "./accounts.controller";
import { DatabaseModule } from "@common/database/database.module";

@Global()
@Module({
  controllers: [AccountsController],
  providers: [AccountsService],
  exports: [AccountsService],
  imports: [DatabaseModule]
})
export class AccountsModule {
}
