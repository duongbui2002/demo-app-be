export enum GenderEnum {
  MALE = "male",
  FEMALE = "female"
}

export const PublicQuerySelection = "avatar username roles";