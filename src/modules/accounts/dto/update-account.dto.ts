import { IsArray, IsBoolean, IsEnum, IsOptional, IsString } from "class-validator";
import { RoleEnum } from "@modules/auth/auth.enum";
import { GenderEnum } from "@modules/accounts/account.enum";

export class UpdateAccountDto {
  @IsOptional()
  @IsBoolean()
  isActivated?: boolean;

  @IsOptional()
  @IsString()
  displayName?: string;

  @IsOptional()
  @IsArray()
  @IsString({ each: true })
  @IsEnum(RoleEnum, { each: true })
  roles?: RoleEnum[];

  @IsOptional()
  @IsEnum(RoleEnum)
  role?: string;

  @IsOptional()
  @IsEnum(RoleEnum)
  removeRole?: string;

  @IsOptional()
  @IsString()
  address?: string;

  @IsOptional()
  @IsEnum(GenderEnum)
  gender?: string;

  @IsOptional()
  @IsString()
  dob?: number;

  @IsOptional()
  @IsString()
  phone?: string;

  @IsOptional()
  @IsString()
  identityNumber?: string;

  @IsOptional()
  @IsString()
  cardId?: string;

  @IsOptional()
  @IsString()
  joinedDate?: string;

  @IsOptional()
  @IsString()
  leavedDate?: string;

  @IsOptional()
  @IsString()
  workType?: string;

  @IsOptional()
  @IsString()
  workModel?: string;
}

export class UpdateSelfAccountDto {
  @IsOptional()
  @IsString()
  displayName: string;

  @IsOptional()
  @IsEnum(GenderEnum)
  gender?: string;
}
