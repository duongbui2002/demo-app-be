import { IsOptional } from "class-validator";
import { Transform } from "class-transformer";

export class FindAccountFilterDto {

  @IsOptional()
  @Transform(({ value }) => new RegExp(value, "i"))
  username?: string;

  @IsOptional()
  @Transform(({ value }) => new RegExp(value, "i"))
  displayName?: string;
}

